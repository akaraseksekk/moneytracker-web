import Vue from 'vue'
import App from './App.vue'
import LoginPage from './login/LoginPage.vue'
import Page from './Category/Page.vue'
import TransantionPage from './Transaction/Page'
import 'bootstrap/dist/css/bootstrap.min.css';
import VueResource from 'vue-resource'

import Axios from 'axios'

const TestPage = {template: '<p>TestPage</p>'}
Vue.config.productionTip = false
Vue.use(VueResource)
const routes = {
  '/': App,
  '/login': LoginPage,
  '/test': TestPage,
  '/page': Page,
  '/transaction':TransantionPage

}

new Vue({
  el: '#app',
  //render: h => h(App)
  routes,
  data: {
    currentRoute: window.location.pathname
  },
  computed: {
    ViewComponent () {
      return routes[this.currentRoute] 
    }
  },
  render (h) { return h(this.ViewComponent) }
})

